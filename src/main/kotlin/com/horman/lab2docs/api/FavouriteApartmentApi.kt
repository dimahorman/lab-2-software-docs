package com.horman.lab2docs.api

import com.horman.lab2docs.dto.FavouriteApartment
import com.horman.lab2docs.service.FavouriteApartmentService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/favourites")
class FavouriteApartmentApi(private val favouriteApartmentService: FavouriteApartmentService) {
    @GetMapping
    fun getAll(): List<FavouriteApartment> {
        return favouriteApartmentService.getAll()
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Long): FavouriteApartment {
        return favouriteApartmentService.getById(id)
    }

    @PutMapping
    fun update(@RequestBody apartment: FavouriteApartment): FavouriteApartment {
        return favouriteApartmentService.createOrUpdate(apartment)
    }

    @PostMapping
    fun create(@RequestBody apartment: FavouriteApartment): FavouriteApartment {
        return favouriteApartmentService.createOrUpdate(apartment)
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Long) {
        favouriteApartmentService.deleteById(id)
    }
}
