package com.horman.lab2docs.api

import com.horman.lab2docs.dto.Apartment
import com.horman.lab2docs.service.ApartmentService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/apartments")
class ApartmentApi(private val apartmentService: ApartmentService) {
    @GetMapping
    fun getAll(): List<Apartment> {
        return apartmentService.getAll()
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Long): Apartment {
        return apartmentService.getById(id)
    }

    @PutMapping
    fun update(@RequestBody apartment: Apartment): Apartment {
        return apartmentService.createOrUpdate(apartment)
    }

    @PostMapping
    fun create(@RequestBody apartment: Apartment): Apartment {
        return apartmentService.createOrUpdate(apartment)
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Long) {
        apartmentService.deleteById(id)
    }
}
