package com.horman.lab2docs.api

import com.horman.lab2docs.dto.User
import com.horman.lab2docs.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/users")
class UserApi(private val userService: UserService) {
    @GetMapping
    fun getAll(): List<User> {
        return userService.getAll()
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable id: String): User {
        return userService.getById(id)
    }

    @PutMapping
    fun update(@RequestBody user: User): User {
        return userService.createOrUpdate(user)
    }

    @PostMapping
    fun create(@RequestBody user: User): User {
        return userService.createOrUpdate(user)
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: String) {
        userService.deleteById(id)
    }
}
