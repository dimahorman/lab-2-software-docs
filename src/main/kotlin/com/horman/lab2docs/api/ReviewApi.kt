package com.horman.lab2docs.api

import com.horman.lab2docs.dto.Review
import com.horman.lab2docs.service.ReviewService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/reviews")
class ReviewApi(private val reviewService: ReviewService) {
    @GetMapping
    fun getAll(): List<Review> {
        return reviewService.getAll()
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Long): Review {
        return reviewService.getById(id)
    }

    @PutMapping
    fun update(@RequestBody review: Review): Review {
        return reviewService.createOrUpdate(review)
    }

    @PostMapping
    fun create(@RequestBody review: Review): Review {
        return reviewService.createOrUpdate(review)
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Long) {
        reviewService.deleteById(id)
    }
}
