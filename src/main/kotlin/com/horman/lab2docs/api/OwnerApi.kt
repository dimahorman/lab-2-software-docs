package com.horman.lab2docs.api

import com.horman.lab2docs.dto.Owner
import com.horman.lab2docs.service.OwnerService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/owners")
class OwnerApi(private val ownerService: OwnerService) {
    @GetMapping
    fun getAll(): List<Owner> {
        return ownerService.getAll()
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable id: String): Owner {
        return ownerService.getById(id)
    }

    @PutMapping
    fun update(@RequestBody owner: Owner): Owner {
        return ownerService.createOrUpdate(owner)
    }

    @PostMapping
    fun create(@RequestBody owner: Owner): Owner {
        return ownerService.createOrUpdate(owner)
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: String) {
        ownerService.deleteById(id)
    }
}
