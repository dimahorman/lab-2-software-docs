package com.horman.lab2docs

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Lab2DocsApplication

fun main(args: Array<String>) {
    runApplication<Lab2DocsApplication>(*args)
}
