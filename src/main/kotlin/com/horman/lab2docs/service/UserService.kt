package com.horman.lab2docs.service

import com.horman.lab2docs.dao.UserDao
import com.horman.lab2docs.dto.User
import com.horman.lab2docs.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class UserService(private val dao: UserDao) {
    companion object {
        private const val RECORD_NAME = "USER"
    }

    fun getAll(): List<User> {
        return dao.findAll()
    }

    fun getById(id: String): User {
        val optional = dao.findById(id)
        return if (optional.isPresent) {
            optional.get()
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun createOrUpdate(user: User): User {
        return dao.save(user)
    }

    fun deleteById(id: String) {
        val optional = dao.findById(id)
        if (optional.isPresent) {
            dao.deleteById(id)
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun deleteAll() {
        dao.deleteAll()
    }
}
