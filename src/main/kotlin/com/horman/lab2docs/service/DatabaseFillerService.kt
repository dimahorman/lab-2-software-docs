package com.horman.lab2docs.service

import com.horman.lab2docs.dto.*
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator
import org.springframework.stereotype.Service
import java.security.SecureRandom
import kotlin.random.Random

@Service
class DatabaseFillerService(
    private val apartmentService: ApartmentService,
    private val userService: UserService,
    private val reviewService: ReviewService,
    private val ownerService: OwnerService,
    private val favouriteApartmentService: FavouriteApartmentService,
) {

    fun fillDatabase() {
        cleanDatabase()
        val users = fillUsers()
        val owners = fillOwners()
        val apartments = fillApartments(owners)
        fillReviews(users, apartments)
        fillFavourites(users, apartments)
    }

    fun cleanDatabase() {
        reviewService.deleteAll()
        favouriteApartmentService.deleteAll()
        apartmentService.deleteAll()
        ownerService.deleteAll()
        userService.deleteAll()
    }

    private fun fillApartments(owners: List<Owner>): List<Apartment> {
        val apartments = mutableListOf<Apartment>()
        repeat(1000) {
            val randOwnerIndex = (owners.indices).random()
            apartments.add(generateApartment(owners[randOwnerIndex].id))
        }
        return apartments
    }


    private fun fillFavourites(users: List<User>, apartments: List<Apartment>){
        repeat(1000) {
            val randApartmentIndex = (apartments.indices).random()
            val randUserIndex = (users.indices).random()
            generateFavourite(apartments[randApartmentIndex].id, users[randUserIndex].id)
        }
    }

    private fun fillOwners(): List<Owner> {
        val ownersList = mutableListOf<Owner>()
        repeat(1000) {
            val owner = generateOwner()
            ownersList.add(owner)
        }
        return ownersList
    }

    private fun fillReviews(users: List<User>, apartments: List<Apartment>) {
        repeat(1000) {
            val randUserIndex = (users.indices).random()
            val randApartmentIndex = (apartments.indices).random()
            generateReview(users[randUserIndex].id, apartments[randApartmentIndex].id)
        }
    }

    private fun fillUsers(): List<User> {
        val usersList = mutableListOf<User>()
        repeat(1000) {
            val user = generateUser()
            usersList.add(user)
        }
        return usersList
    }

    private fun generateApartment(ownerId: String): Apartment {
        val id = SecureRandom().nextLong()
        val address = Base64StringKeyGenerator().generateKey()
        val photoUrl = Base64StringKeyGenerator().generateKey()
        val isAvailable = true
        val apartment = Apartment(id, ownerId, address, photoUrl, isAvailable)
        return apartmentService.createOrUpdate(apartment)
    }

    private fun generateFavourite(apartmentId: Long, userId: String): FavouriteApartment {
        val id = SecureRandom().nextLong()
        val favourite = FavouriteApartment(id, apartmentId, userId)
        return favouriteApartmentService.createOrUpdate(favourite)
    }

    private fun generateOwner(): Owner {
        val id = Base64StringKeyGenerator().generateKey()
        val firstName = Base64StringKeyGenerator().generateKey()
        val lastName = Base64StringKeyGenerator().generateKey()
        val address = Base64StringKeyGenerator().generateKey()
        val photoUrl = Base64StringKeyGenerator().generateKey()
        val phoneNumber = Base64StringKeyGenerator().generateKey()
        val owner = Owner(id, firstName, lastName, address, phoneNumber, photoUrl)
        return ownerService.createOrUpdate(owner)
    }

    private fun generateReview(userId: String, apartmentId: Long): Review {
        val id = SecureRandom().nextLong()
        val text = Base64StringKeyGenerator().generateKey()
        val rating = Random.nextDouble(1.0, 5.0)
        val review = Review(id, userId, apartmentId, text, rating)
        return reviewService.createOrUpdate(review)
    }

    private fun generateUser(): User {
        val id = Base64StringKeyGenerator().generateKey()
        val firstName = Base64StringKeyGenerator().generateKey()
        val lastName = Base64StringKeyGenerator().generateKey()
        val photoUrl = Base64StringKeyGenerator().generateKey()
        val phoneNumber = Base64StringKeyGenerator().generateKey()
        val user = User(id, firstName, lastName, phoneNumber, photoUrl)
        return userService.createOrUpdate(user)
    }
}
