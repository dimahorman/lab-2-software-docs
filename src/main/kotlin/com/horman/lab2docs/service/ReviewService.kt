package com.horman.lab2docs.service

import com.horman.lab2docs.dao.ReviewDao
import com.horman.lab2docs.dto.Review
import com.horman.lab2docs.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class ReviewService(private val dao: ReviewDao) {
    companion object {
        private const val RECORD_NAME = "REVIEW"
    }

    fun getAll(): List<Review> {
        return dao.findAll()
    }

    fun getById(id: Long): Review {
        val optional = dao.findById(id)
        return if (optional.isPresent) {
            optional.get()
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun createOrUpdate(review: Review): Review {
        return dao.save(review)
    }

    fun deleteById(id: Long) {
        val optional = dao.findById(id)
        if (optional.isPresent) {
            dao.deleteById(id)
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun deleteAll() {
        dao.deleteAll()
    }
}
