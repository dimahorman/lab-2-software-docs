package com.horman.lab2docs.service

import com.horman.lab2docs.dao.FavouriteApartmentDao
import com.horman.lab2docs.dto.FavouriteApartment
import com.horman.lab2docs.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class FavouriteApartmentService(private val dao: FavouriteApartmentDao) {
    companion object {
        private const val RECORD_NAME = "FAVOURITE_APARTMENT"
    }

    fun getAll(): List<FavouriteApartment> {
        return dao.findAll()
    }

    fun getById(id: Long): FavouriteApartment {
        val optional = dao.findById(id)
        return if (optional.isPresent) {
            optional.get()
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun createOrUpdate(apartment: FavouriteApartment): FavouriteApartment {
        return dao.save(apartment)
    }

    fun deleteById(id: Long) {
        val optional = dao.findById(id)
        if (optional.isPresent) {
            dao.deleteById(id)
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun deleteAll() {
        dao.deleteAll()
    }
}
