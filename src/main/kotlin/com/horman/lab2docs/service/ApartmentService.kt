package com.horman.lab2docs.service

import com.horman.lab2docs.dao.ApartmentDao
import com.horman.lab2docs.dto.Apartment
import com.horman.lab2docs.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class ApartmentService(private val dao: ApartmentDao) {
    companion object {
        private const val RECORD_NAME = "APARTMENT"
    }

    fun getAll(): List<Apartment> {
        return dao.findAll()
    }

    fun getById(id: Long): Apartment {
        val optional = dao.findById(id)
        return if (optional.isPresent) {
            optional.get()
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun createOrUpdate(apartment: Apartment): Apartment {
        return dao.save(apartment)
    }

    fun deleteById(id: Long) {
        val optional = dao.findById(id)
        if (optional.isPresent) {
            dao.deleteById(id)
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun deleteAll() {
        dao.deleteAll()
    }
}
