package com.horman.lab2docs.service

import com.horman.lab2docs.dao.OwnerDao
import com.horman.lab2docs.dto.Owner
import com.horman.lab2docs.exception.RecordNotFound
import org.springframework.stereotype.Service

@Service
class OwnerService(private val dao: OwnerDao) {
    companion object {
        private const val RECORD_NAME = "OWNER"
    }

    fun getAll(): List<Owner> {
        return dao.findAll()
    }

    fun getById(id: String): Owner {
        val optional = dao.findById(id)
        return if (optional.isPresent) {
            optional.get()
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun createOrUpdate(owner: Owner): Owner {
        return dao.save(owner)
    }

    fun deleteById(id: String) {
        val optional = dao.findById(id)
        if (optional.isPresent) {
            dao.deleteById(id)
        } else {
            throw RecordNotFound(RECORD_NAME)
        }
    }

    fun deleteAll() {
        dao.deleteAll()
    }
}
