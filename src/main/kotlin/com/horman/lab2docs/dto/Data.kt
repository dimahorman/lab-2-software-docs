package com.horman.lab2docs.dto

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "users")
data class User(
    @Id @Column(name = "id")
    val id: String,
    @Column(name = "first_name")
    val firstName: String,
    @Column(name = "last_name")
    val lastName: String,
    @Column(name = "phone_number")
    val phoneNumber: String,
    @Column(name = "photo_url")
    val photoUrl: String,
)

@Entity
@Table(name = "favourites")
data class FavouriteApartment(
    @Id @Column(name = "id")
    val id: Long,
    @Column(name = "apartment_id")
    val apartmentId: Long,
    @Column(name = "user_id")
    val userId: String
)

@Entity
@Table(name = "owners")
data class Owner(
    @Id @Column(name = "id")
    val id: String,
    @Column(name = "first_name")
    val firstName: String,
    @Column(name = "last_name")
    val lastName: String,
    @Column(name = "address")
    val address: String,
    @Column(name = "phone_number")
    val phoneNumber: String,
    @Column(name = "photo_url")
    val photoUrl: String
)

@Entity
@Table(name = "apartments")
data class Apartment(
    @Id @Column(name = "id")
    val id: Long,
    @Column(name = "owner_id")
    val ownerId: String,
    @Column(name = "address")
    val address: String,
    @Column(name = "photo_url")
    val photoUrl: String,
    @Column(name = "is_available")
    val isAvailable: Boolean
)

@Entity
@Table(name = "reviews")
data class Review(
    @Id @Column(name = "id")
    val id: Long,
    @Column(name = "user_id")
    val userId: String,
    @Column(name = "apartment_id")
    val apartmentId: Long,
    @Column(name = "text")
    val text: String,
    @Column(name = "rating")
    val rating: Double
)
