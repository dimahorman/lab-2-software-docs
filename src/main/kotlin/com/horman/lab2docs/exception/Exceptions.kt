package com.horman.lab2docs.exception

open class BusinessException(
    open val code: String,
    open val description: String
) : Throwable("$code: $description", null, true, false)

open class RecordNotFound(recordName: String): BusinessException("RECORD_NOT_FOUND", "Record $recordName of given id was not found")
