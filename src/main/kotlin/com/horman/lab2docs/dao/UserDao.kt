package com.horman.lab2docs.dao

import com.horman.lab2docs.dto.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserDao : JpaRepository<User, String >

