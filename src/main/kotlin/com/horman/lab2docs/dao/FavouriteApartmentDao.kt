package com.horman.lab2docs.dao

import com.horman.lab2docs.dto.FavouriteApartment
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FavouriteApartmentDao: JpaRepository<FavouriteApartment, Long>
