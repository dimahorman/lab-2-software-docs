package com.horman.lab2docs.dao

import com.horman.lab2docs.dto.Apartment
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ApartmentDao : JpaRepository<Apartment, Long>

