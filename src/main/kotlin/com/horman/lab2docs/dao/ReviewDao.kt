package com.horman.lab2docs.dao

import com.horman.lab2docs.dto.Review
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ReviewDao : JpaRepository<Review, Long>
